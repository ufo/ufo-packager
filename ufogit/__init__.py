#
# ufo-packager
#
# Copyright (C) 2010, Agorabox.
#
# Author: Sylvain BAUBEAU <sylvain.baubeau@agorabox.org>
#
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.
#

import os
import subprocess
from git import *
import git.config
from ConfigParser import ConfigParser
from optparse import OptionParser
import io
import logging
from utils import call
import bz2
import glob
import re
import pdb
import tempfile
import shutil
import urllib

logging.basicConfig(level=logging.DEBUG)

LOOKASIDE = 'http://pkgs.fedoraproject.org/repo/pkgs'
GITORIOUSURL = "git@gitorious.alpha.agorabox.org"
UPLOADSERVER = "kickstart.alpha.agorabox.org"
GENREPOCMD = "createrepo"
FEDORAGITWEB = "http://pkgs.fedoraproject.org/gitweb/?p=%s.git;a=summary"
UFOCONFCOMMITMSG = "Added ufo.conf"

ufoversion = ""
fedoraversion = ""
ufogit = None
arch = "i386"

options = None

ufo_to_fedora = {
    "ufo2" : "f12",
    "ufo3" : "f15"
}

class UFOPkgOptionParser(OptionParser):
    def __init__(self, usage, description, version):
        OptionParser.__init__(self, usage=usage, description=description, version=version)
        self.add_option("-r", "--repository", dest="repository", default="",
                        help="specify git repository", metavar="PATH")
        self.add_option("-a", "--archive", dest="archive", default="",
                        help="build archive using either : git, git-dirty, make-dist or fedora", metavar="ARCHIVE-METHOD")
        self.add_option("-s", "--sources", dest="sources", default="",
                        help="local path to the sources", metavar="PATH")
        self.add_option("-t", "--tag", dest="tag", default="",
                        help="specify a tag for the sources repository", metavar="PATH")
        self.add_option("--ufo", dest="version", default="",
                        help="specify the UFO version", metavar="VERSION")
        self.add_option("-n", "--no-fetch", dest="fetch", action="store_false", default=True,
                        help="do not fetch remote repositories")

    def parse_args(self):
        global options
        (options, args) = OptionParser.parse_args(self)
        if not options.repository:
            options.repository = os.getcwd()
        global ufoversion
        global fedoraversion
        if options.version:
            ufoversion = "ufo" + options.version
            fedoraversion = ufo_to_fedora[ufoversion]
        return options, args

class SafeConfigParser(ConfigParser):
    def get(self, section, key, default=""):
        try:
            return ConfigParser.get(self, section, key)
        except:
            return default

class Remote(object):
    def fetch(self):
        logging.debug("Fetch in not implemented for backend %s" % self.__class__.__name__)

    def archive(self, *args):
        logging.debug("Archive in not implemented for backend %s" % self.__class__.__name__)

class TarballRemote(Remote):
    def __init__(self, repo, name, url, cvstype):
        self.url = url

    def fetch(self):
        if not os.path.exists(os.path.basename(self.url)):
            urllib.urlretrieve(self.url, os.path.basename(self.url))

    def archive(self, archive, ref):
        self.fetch()

class BazaarRemote(Remote):
    def __init__(self, repo, name, url, cvstype):
        sources = os.path.join(ufogit.path, "sources")
        self.path = path = os.path.join(sources, "upstream")
        if not os.path.exists(sources):
            os.makedirs(sources)
        if not os.path.exists(path):
            call(["bzr", "branch", url, "upstream"], cwd=sources)

    def archive(self, archive, ref):
        archive.close()
        call(["bzr", "export", os.path.join(ufogit.path, archive.name)], cwd=self.path)

class GitRemote(Remote):
    def __init__(self, repo, name, url="", cvstype="git"):
        self.name = name
        self.repository = repo
        self.url = url
        self.cvstype = cvstype
        self.branch = None
        name = "remote-" + name
        if not hasattr(repo.remotes, name):
            self.remote = repo.create_remote(name, url)
        else:
            self.remote = getattr(repo.remotes, name)
        if options.fetch:
            self.fetch()
        if ufogit.config.has_section(self.config_section):
            for option in ufogit.config.options(self.config_section):
                setattr(self, option, ufogit.config.get(self.config_section, option))
    
    def fetch(self):
        logging.debug("Fetching " + self.name)
        try:
            self.repository.git.fetch(self.remote.name)
            self.repository.git.fetch(self.remote.name, tags=True)
            # self.remote.fetch()
        except AssertionError, e:
            logging.error("Failed to fetch " + self.name)

    def push(self):
        logging.info("Pushing %s to %s.%s" % (self.get_branch_name(ufoversion), self.remote.name, ufoversion)) 
        self.repository.git.push(self.remote.name, "%s:%s" % (self.get_branch_name(ufoversion), ufoversion))

    def checkout(self, local, remote="", tag=""):
        if remote and '/' not in remote: remote = '%s/%s' % (self.remote.name, remote)
        if tag: remote = tag
        if '/' not in local: local = '%s/%s' % (self.name, local)
        logging.info("Checking out %s into %s" % (remote, local))
        if not hasattr(self.repository.heads, local):
            self.repository.git.checkout(remote, b=local)
        branch = getattr(self.repository.heads, local)
        if not self.branch:
            self.branch = branch
        branch.checkout()
        return branch

    def autotools(self):
        pass

    def get_spec_patches(self, branch):
        patches = {}
        spec = self.get_file(branch, ufogit.spec).readlines()
        pattern = re.compile("^Patch(\d*)(\s*):(\s*)((.*).patch)")
        applypattern = re.compile("^%patch(\d*).*(\-p(\d)).*")
        for line in spec:
            result = pattern.search(line)
            if result:
                patches[int(result.group(1))] = [ result.group(4) ]

            result = applypattern.search(line)
            if result:
                patches[int(result.group(1))].append(int(result.group(3)))
        return patches

    def apply_patches(self, branch, stgit=True):
        patches = self.get_spec_patches(branch)
        patches = patches.items()
        patches.sort()
        # if os.path.exists("autogen.sh"):
        #     call([ "/bin/sh", "autogen.sh" ], cwd=ufogit.path)
        #     call([ "make", "dist" ], cwd=ufogit.path)
        if stgit:
            call([ "stg", "init" ], cwd=ufogit.path)
        for k, v in patches:
            tmppatch = os.path.join(tempfile.gettempdir(), v[0])
            open(tmppatch, 'w').write(ufogit.get_file(branch, v[0]).read())
            if stgit:
                call([ "stg", "import", tmppatch ], cwd=ufogit.path)
            else:
                call([ "patch", "-p" + str(v[1]) ], input=ufogit.get_file(branch, v[0]).read(), cwd=ufogit.path)
            # Uncomment this to save the patches
            # open(v[0], 'w').write(os.path.join(ufogit.path, ufogit.get_file(branch, v[0]).read()))

    def get_tag_from_version(self, version, release=""):
        wantedtag = getattr(self, "tagformat", "refs/tags/{version}").format(name=ufogit.package, version=version, release=release)
        if wantedtag in [ tag.path for tag in self.repository.tags ]:
            return wantedtag
        wantedtag2 = "%s-%s-%s" % (ufogit.package, version, release)
        if wantedtag2 in [ tag.path for tag in self.repository.tags ]:
            return "%s-%s-%s" % (ufogit.package, version, release)
        logging.critical("Could not find tag (tried %s and %s)" % (wantedtag, wantedtag2))
        return ""

    def archive(self, archive, ref):
        if not '/' in ref: ref = "%s/%s" % (self.remote.name, ref)
        logging.info("Generating %s from %s" % (archive.name, ref))
        # For some reason, the generated archive is the same as the main repository
        # Or maybe I'm just crazy
        # self.repository.archive(archive, ref, prefix=ufogit.get_nevra() + "/")
        output = call(["git", "archive", "--format=tar", "--prefix=" + ufogit.get_nevra() + "/", ref], cwd=self.repository.working_dir, output=True)[1]
        archive.write(output)
        archive.close()

    def get_branch_name(self, name):
        if '/' not in name: name = "%s/%s" % (self.name, name)
        return name

    def get_branch(self, name):
        if '/' not in name: name = "%s/%s" % (self.name, name)
        try:
             return getattr(self.repository.heads, name)
        except:
             logging.warning("Could not find branch %s in repository %s" % (name, self.name))
             raise

    def get_file(self, branch, path):
        if type(branch) == str:
            tree = self.get_branch(branch).commit.tree
        else:
            tree = branch.commit.tree
        for entry in path.split('/'):
            tree = tree[entry]
        return io.BytesIO(tree.data)

    def clone_all(self):
        self.checkout("master", "master")
        version, release = ufogit.get_version(), ufogit.get_release()
        tag = self.get_tag_from_version(version, release)
        if not tag:
            logging.warning("Could not find a tag for %s, release %s" % (version, release))
            ref = "master"
            tag = "master"
        else:
            ref = "refs/tags/%s" % os.path.basename(tag)
        self.checkout("upstream/" + tag, ref)

        if ufogit.fedora:
            self.apply_patches(ufogit.fedora.get_branch(fedoraversion))

    def get_current_branch(self):
        tag = self.get_tag_from_version(ufogit.get_version(), ufogit.get_release())
        branch = self.get_branch(tag)
        if not branch:
            return self.checkout(ufogit.get_version(), "refs/tags/" + tag)

class SVNRemote(GitRemote):
    def __init__(self, repo, name, url="", cvstype="git"):
        Remote.__init__(self, repo, name, url, cvstype)
        if self.cvstype == "svn":
            call(["git", "config", "--add", "remote.%s.fetch" % "remote-ufo-upstream", "+refs/remotes/*:refs/remotes/*"], cwd=ufogit.path)
            call(["git", "fetch", "remote-ufo-upstream"], cwd=ufogit.path)
            call(["git", "svn", "init", "-t", "tags", "-b", "branches", "-T", "trunk", self.url], cwd=ufogit.path)
            # For some reason, we have to call it twice
            call(["git", "svn", "init", "-t", "tags", "-b", "branches", "-T", "trunk", self.url], cwd=ufogit.path)

class UFOPkgGit(GitRemote):
    config_section = "ufo-pkg"

    def __init__(self, repo, name, package):
        super(UFOPkgGit, self).__init__(repo, name, self.get_gitorious_url("packages", package))
        try:
            self.checkout(ufoversion, ufoversion)
        except:
            logging.warning("Could not find a remote branch %s/%s" % (self.remote.name, ufoversion))

    def get_gitorious_url(self, project, package):
        return "%s:%s/%s.git" % (GITORIOUSURL, project, package)

    def clone_all(self):
        pass

    def get_spec(self, branch):
        return self.get_file(branch, ufogit.spec)

    def generate_spec(self):
        ufopatches = ufogit.ufoupstream.generate_patches()
        specpatches = self.get_spec_patches(ufoversion)
        spec = self.get_spec(ufoversion).read()
        lastpatch = max(specpatches.keys())
        index = spec.find("Patch%d" % lastpatch)
        index = spec.find("\n", index)
        if index == -1:
            raise "Invalid specfile"
        s = ""
        patchindex = lastpatch + 1
        for patch in ufopatches:
            s += "Patch%d: %s.patch\n" % (patchindex, patch)
            patchindex += 1
        spec = spec[:index + 1] + s + spec[index + 1:]

        index = spec.find("%%patch%d" % lastpatch)
        index = spec.find("\n", index)
        if index == -1:
            raise "Invalid specfile"
        s = ""
        patchindex = lastpatch + 1
        for patch in ufopatches:
            s += "%%patch%d -p1 -b .%s\n" % (patchindex, patch)
            patchindex += 1
        spec = spec[:index + 1] + s + spec[index + 1:]

class UFOUpstreamGit(GitRemote):
    config_section = "ufo"
    
    def __init__(self, repo, name, url="", cvstype="git"):
        if options.sources:
            repo = Repo(options.sources)
        super(UFOUpstreamGit, self).__init__(repo, name, url, cvstype)

    def stgit_init(self, branch):
        shutil.rmtree(os.path.join(ufogit.path, ".git", "patches"), ignore_errors=True)
        config_section = 'branch "%s.stgit"' % self.get_branch_name(branch)
        import pdb; pdb.set_trace()
        ufogit.gitconfig.remove_section(config_section)
        call([ "stg", "init" ], cwd=ufogit.path)

    def generate_patches(self):
        if not ufogit.fedora:
            logging.debug("We do not apply patches when there is no Fedora git, that's a rule")
        # patches = self.repository.index.diff('upstream/initscripts-9.02.2-1', create_patch=True)
        patches = []
        current_branch = ufogit.upstream.get_current_branch()
        commits = self.repository.iter_commits(current_branch.name + ".." + self.get_branch_name(ufoversion))

        self.checkout(ufoversion)
        shutil.rmtree(os.path.join(ufogit.path, "ufo-patches"), ignore_errors=True)
        self.stgit_init(ufoversion)
        
        for commit in commits:
            # self.repository.git.show(commit.sha)
            call(["stg", "uncommit", commit.sha], cwd=ufogit.path)
            # patches.append(call(["stg", "top"], output=True, cwd=ufogit.path)[1].strip())
        call(["stg", "export", "-p", "-d", "ufo-patches"], cwd=ufogit.path)
        output = call(["stg", "series"], cwd=ufogit.path, output=True)[1].split("\n")
        for line in output:
            if line:
                patches.append(line[2:])
        return patches

    def apply_patches(self):
        self.checkout("with-%s-patches" % fedoraversion, "master")
        Remote.apply_patches(self, ufogit.fedora.get_branch(fedoraversion))

    def clone_all(self):
        try:
            self.get_branch(ufoversion)
            self.checkout(ufoversion, ufoversion)
        except:
            self.checkout(ufoversion, "master")

class FedoraGit(GitRemote):
    config_section = "fedora"

    def clone_all(self):
        self.checkout(fedoraversion, fedoraversion)
    
    def checkout(self, local, remote=""):
        if remote and '/' not in remote: remote = self.get_remote_master()
        if '/' not in local: local = '%s/%s' % (self.name, local)
        Remote.checkout(self, local, remote)

    def check_new_version(self):
        self.checkout(fedoraversion)
        self.repository.git.rebase(self.get_remote_master())

    def get_remote_master(self):
        return '%s/%s' % (self.remote.name, fedoraversion)

    def __new__(cls, repo, name, url="", cvstype="git"):
        if not options.fetch or urllib.urlopen(FEDORAGITWEB % ufogit.package).code != 404:
            return super(FedoraGit, cls).__new__(cls, repo, name, url, cvstype)
        else:
            raise Exception("Could not find the Fedora GIT repository")

class Upstream(object):
    def __new__(self, repo, name, url="", cvstype="git"):
        if not cvstype:
           cvstype = url.split(':')[0]
        if cvstype == "git":
            base = GitRemote
        elif cvstype == "bzr":
            base = BazaarRemote
        elif cvstype == "tarball":
            base = TarballRemote
        else:
            raise Exception("Unhandled versioning system : %s" % cvstype)
        return type(cvstype.upper() + 'Upstream', (base,), { "config_section" : "upstream" })(repo, name, url, cvstype)

    def __init__(self, repo, name, url="", cvstype="git"):
        if options.sources and not ufogit.ufoupstream:
            repo = Repo(options.sources)
        super(Upstream, self).__init__(repo, name, url, cvstype)

def get_cvs_backend(cvstype):
    if cvstype == "git":
        base = GitRemote
    elif cvstype == "bzr":
        base = BazaarRemote
    else:
        return Exception("Unhandled versioning system : %s" % cvstype)
    return type('Upstream', (Upstream, base,), {})

class MetaGit(object):
    def __init__(self, path, package=""):
        global ufogit
        global ufoversion
        global fedoraversion
        ufogit = self
        if not package:
            package = os.path.basename(path)
        self.package = package
        self.path = path
        self.spec = package + ".spec"
        self.fedora = None
        self.ufoupstream = None
        self.ufopkg = None
        self.upstream = None
        self.options = options
        self.sources_git = None

        try:
            self.repository = Repo(self.path)
        except:
            if not os.path.exists(self.package):
                os.makedirs(self.path)
                self.repository = Repo.init(self.path)
                init = True
            else:
                init = False
            self.repository = Repo(self.path)
            if init:
                self.repository.init(self.path)

        if not ufoversion:
            try:
                ufoversion = self.repository.active_branch.name.split('/')[-1]
                fedoraversion = ufo_to_fedora[ufoversion]
            except:
                raise Exception("Failed to determine the UFO version from the active branch name")

        self.dist = "." + ufoversion
        self.distvar = "ufo"
        self.distval = ufoversion[3:]
        self.distshort = ufoversion

        self.rpmdefines = ["--define", "_sourcedir %s" % path,
                           "--define", "_specdir %s" % path,
                           "--define", "_builddir %s" % path,
                           "--define", "_srcrpmdir %s" % os.path.join(path, "srpms"),
                           "--define", "_rpmdir %s" % os.path.join(path, "rpms"),
                           "--define", "dist %s" % self.dist,
                           "--define", "%s %s" % (self.distvar, self.distval),
                           "--define", "%s 1" % self.distshort]

        self.load_config()
        
    def clone(self):
        subprocess.call([ "git", "clone", self.get_gitorious_url("ufo", self.package) ])

    def clone_all(self):
        if self.fedora:
            self.fedora.clone_all()
        if self.upstream:
            self.upstream.clone_all()
        if self.ufoupstream:
            self.ufoupstream.clone_all()

    def get_gitconfig(self):
        return git.config.GitConfigParser(os.path.join(".git/config"), read_only=False)
    gitconfig = property(get_gitconfig)

    def create_config(self, cp):
        input = None
        while input not in [ '', 'git', 'svn', 'mercurial', 'cvs', 'bzr', 'tarball' ]:
            input = raw_input("Please enter the CVS program used by upstream.\nIt can "
                              "be git, svn, mercurial, bzr, cvs, tarball or leave blank if UFO is upstream : ")
        if input:
            cp.add_section("upstream")
            cp.set("upstream", "cvstype", input)
            input = None
            while not input:
                input = raw_input("Enter the URL of the repository (ex: git://git.gnome.org/nautilus) : ")
                cp.set("upstream", "url", input)

            input = raw_input("Specify the upstream tag format (default: %version) :")
            if input:
                cp.set("upstream", "tagformat", input)

        input = raw_input("Please enter the UFO gitorious URL of the package, leave\n"
                          "blank if we don't have a dedicated git with the sources\n"
                          "of the program (ex: git://gitorious.alpha.agorabox.org/ufo/nautilus.git) : ")
        if input:
            cp.add_section("ufo")
            cp.set("ufo", "url", input)

            input = raw_input("Please enter the name of the name of the branch (gitorious) : ")
            if input:
                cp.set("ufo", "branch", input)

        input = ""
        while input not in [ 'Y', 'N' ]:
            input = raw_input("Is this package available on Fedora ? (Y/N) : ").upper()

        if input == "Y":
            cp.add_section("fedora")

        input = raw_input("Specify the path to the spec file to import : ")
        if input:
            shutil.copy2(input, self.path)

        self.save_config(commit=True)
        self.clone_all()

    def load_config(self, reload=False):
        self.config = cp = SafeConfigParser()
        ufo_conf = os.path.join(self.path, "ufo.conf")
        if not os.path.exists(ufo_conf):
            self.create_config(cp)
        cp.read(os.path.join(self.path, "ufo.conf"))
        self.ufopkg = UFOPkgGit(self.repository, "ufo-pkg", self.package)

        # tree = self.ufopkg.get_branch(ufoversion).commit.tree
        # cp.readfp(io.BytesIO(tree["ufo.conf"].data))

    def add_to_git(self, file):
        logging.debug("Adding %s to the index." % file)
        self.repository.index.add([ file ])

    def save_config(self, commit=False):
        if len(self.repository.heads):
            self.ufopkg.checkout(ufoversion)
        else:
            open(os.path.join(self.path, ".git", "HEAD"), "w").write("ref: refs/heads/ufo-pkg/%s" % ufoversion)
        
        self.config.write(open(os.path.join(self.path, "ufo.conf"), "w"))
        self.add_to_git("ufo.conf")
        if os.path.exists(os.path.join(self.path, self.spec)):
            self.add_to_git(self.spec)
        # if commit:
        #     self.repository.index.commit(UFOCONFCOMMITMSG)

    def setup(self):
        if self.config.has_section("fedora"):
            self.fedora = FedoraGit(self.repository, 'fedora', "git://pkgs.fedoraproject.org/" + self.package + ".git", "git")
            self.fedora.force = eval(self.config.get("fedora", "force", "False"))
            if not os.path.exists(os.path.join(self.path, "sources")):
                logging.debug("Creating %s/%s from %s/%s" % (self.ufopkg.name, ufoversion, self.fedora.name, fedoraversion))
                self.ufopkg.checkout(ufoversion, "%s/%s" % (self.fedora.remote.name, fedoraversion))
            else:
                if not len(self.repository.branches):
                    logging.debug("Creating branch %s" % ufoversion)
                    self.repository.create_head(ufoversion)

        if self.config.has_section("ufo"):
            self.ufoupstream = UFOUpstreamGit(self.repository, "ufo-upstream", self.config.get("ufo", "url"))
            self.sources_git = self.ufoupstream

            if not self.config.has_section("upstream"):
                self.upstream = Upstream(self.repository, "upstream", self.config.get("ufo", "url"))

        if self.config.has_section("upstream"):
            self.upstream = Upstream(self.repository, "upstream", self.config.get("upstream", "url"), self.config.get("upstream", "cvstype"))
            if not self.sources_git:
                self.sources_git = self.upstream

        if not self.sources_git:
            self.sources_git = self.ufopkg

    def get_file(self, branch, path):
        if type(branch) == str:
            tree = getattr(self.repository.heads, branch).commit.tree
        else:
            tree = branch.commit.tree
        for entry in path.split('/'):
            tree = tree[entry]
        return io.BytesIO(tree.data)

    def get_srpm(self):
        srpm = os.path.join(self.path, "srpms",
                            "%s-%s-%s.src.rpm" % (self.package,
                                                  self.get_version(), self.get_release()))
        return srpm

    def get_nevra(self):
        return "%s-%s" % (self.package, self.get_version())

    def get_spec(self, branch):
        spec = tempfile.NamedTemporaryFile()
        spec.write(self.ufopkg.get_file(branch, self.spec).read())
        spec.flush()
        return spec

    def get_version(self):
        spec = self.get_spec(ufoversion)
        return call([ "rpm", "-q", "--qf", '%{VERSION} "', "--specfile", spec.name ], log=False, output=True)[1].split()[0]

    def get_release(self):
        spec = self.get_spec(ufoversion)
        return call([ "rpm", "-q", "--qf", '%{RELEASE} "', "--specfile", spec.name ], log=False, output=True)[1].split()[0]

    def get_full_version(self):
        return "%s.%s" % (self.get_version(), self.get_release())

    def generate_tarball(self, format, dirty=False):
        archive = bz2.BZ2File(self.package + "-" + self.get_version() + ".tar." + format, "w")
        if dirty:
            logging.debug("Generating tarball from a dirty git is not supported yet...")
        else:
            if self.ufoupstream:
                repo = self.ufoupstream
            elif self.upstream:
                repo = self.upstream
            else:
                repo = self.ufopkg
            if options.tag:
                ref = tag
            else:
                if hasattr(repo, "tagformat"):
                    ref = repo.get_tag_from_version(ufogit.get_version(), ufogit.get_release())
                else:
                    if self.config.has_option(repo.config_section, "branch"):
                        ref = self.config.get(repo.config_section, "branch")
                    else:
                        ref = ufoversion
            self.sources_git.archive(archive, ref) # self.get_nevra())

    def sources(self, format="bz2"):
        if not self.options.archive:
            if self.fedora and (not self.ufoupstream or self.fedora.force):
                self.options.archive = "fedora"
            else:
                self.options.archive = "git"

        if self.options.archive == "git":
            self.generate_tarball(format)
        elif self.options.archive == "git-dirty":
            self.generate_tarball(format, dirty=True)
        elif self.options.archive == "make-dist":
            self.upstream.checkout()
            call([ "make", "dist" ])
            self.ufopkg.checkout()
        elif self.options.archive == "fedora":
            archives = self.ufopkg.get_file(ufoversion, "sources").readlines()

            for archive in archives:
                csum, file = archive.split()
                # See if we already have a valid copy downloaded
                outfile = os.path.join(os.getcwd(), file)
                if os.path.exists(outfile):
                    continue
                    # if _verify_file(outfile, csum, LOOKASIDEHASH):
                    #     continue
                url = '%s/%s/%s/%s/%s' % (LOOKASIDE, self.package, file, csum,
                                          file)
                command = ['curl', '-H',  'Pragma:', '-O', '-R', '-S',  '--fail',
                           '--show-error', url]
                call(command)
    
    def build(self, binary = True, source = True, stage = ""):
        self.rpm(binary = binary, source = source, stage = stage)

    def rpm(self, binary, source, stage):
        if stage:
            build_type = stage
        elif binary and source:
            build_type = "a"
        elif binary:
            build_type = "b"
        else:
            build_type = "s"
        self.sources()
        spec = self.get_spec(ufoversion)
        cmd = [ "rpmbuild" ] + self.rpmdefines + [ "-b" + build_type, self.spec ]
        call(cmd, output=True, log=True)[1]

    def upload(self, repo="testing"):
        path = "/var/www/html/yum/%s/%s/%s" % (repo, filter(str.isdigit, ufoversion), arch)
        call(["find", "rpms", "-name", "*.rpm", "-exec", "scp", "{}", "%s@%s:%s"
            % (os.environ["USER"], UPLOADSERVER, path), ";"])
        call(["ssh", UPLOADSERVER, GENREPOCMD, path ])

    def koji(self, scratch=False):
        cmd = [ "koji", "build" ]
        if scratch:
            cmd += [ "--scratch" ]
        cmd += [ "dist-" + ufoversion, self.get_srpm() ]
        call([ cmd ])

