# -*- coding: utf-8 -*-

# UFO-launcher - A multi-platform virtual machine launcher for the UFO OS
#
# Copyright (c) 2008-2009 Agorabox, Inc.
#
# This is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA. 


import logging
import logging.handlers
import sys
import subprocess
import os, os.path

def call(cmds, env = None, shell = False, cwd = None, output = False, input = None, fork=True, spawn=False, log=True, preexec_fn=None):
    if type(cmds[0]) == str:
        cmds = [ cmds ]
    lastproc = None
    for i, cmd in enumerate(cmds):
        if log: logging.debug(" ".join(cmd) + " with environment : " + str(env))
        if lastproc:
            stdin = lastproc.stdout
        else:
            if input:
                stdin = subprocess.PIPE
            else:
                stdin = None
        stdout = None
        preexec_fn_ = None
        if (len(cmds) and i != len(cmds) - 1):
            stdout = subprocess.PIPE
        if output and i == len(cmds) - 1:
            stdout = subprocess.PIPE
            preexec_fn_ = preexec_fn
        if fork:
            proc = subprocess.Popen(cmd, env=env, shell=shell, cwd=cwd, stdin=stdin, stdout=stdout, preexec_fn=preexec_fn_)
        else:
            proc = subprocess.Popen(cmd, env=env, shell=shell, cwd=cwd, stdin=stdin, stdout=stdout, preexec_fn_=preexec_fn_, fork=fork)
        lastproc = proc

    if spawn:
        return lastproc
    elif output or len(cmds) > 1:
        output = lastproc.communicate()[0]
        if log: logging.debug("Returned : " + str(lastproc.returncode))
        return lastproc.returncode, output
    elif input:
        lastproc.communicate(input)[0]
        if log: logging.debug("Returned : " + str(lastproc.returncode))
        return lastproc.returncode
    else:
        retcode = lastproc.wait()
        if log: logging.debug("Returned : " + str(retcode))
        return retcode
