from distutils.core import setup

commands = [ "ufopkg-build", "ufopkg-clone", "ufopkg-fedora", "ufopkg-import",
             "ufopkg-koji", "ufopkg-push", "ufopkg-upload", "ufopkg-upstream" ]

setup(name='ufo-packager',
      version='0.1',
      packages=['ufogit'],
      scripts=['ufopkg'],
      data_files=[('/usr/libexec/ufopkg', commands)]
)
                                                                 